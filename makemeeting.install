<?php

/**
 * @file
 * Makemeeting installation schemas
 */

/**
 * Implements hook_field_schema().
 */
function makemeeting_field_schema($field) {
  return array(
    'columns' => array(
      'closed' => array(
        'type' => 'int',
        'length' => 1,
        'not null' => FALSE,
        'default' => 0,
      ),
      'hidden' => array(
        'type' => 'int',
        'length' => 1,
        'not null' => FALSE,
        'default' => 0,
      ),
      'one_option' => array(
        'type' => 'int',
        'length' => 1,
        'not null' => FALSE,
        'default' => 0,
      ),
      'limit' => array(
        'type' => 'int',
        'not null' => FALSE,
        'default' => 0,
      ),
      'yesnomaybe' => array(
        'type' => 'int',
        'length' => 1,
        'not null' => FALSE,
        'default' => 0,
      ),
      'choices' => array(
        'type' => 'blob',
        'not null' => FALSE,
        'size' => 'big',
        'serialize' => TRUE,
      ),
      'timezone' => array(
        'type'   => 'varchar',
        'length' => 25,
      ),
    ),
  );
}

/**
 * Implements hook_schema().
 */
function makemeeting_schema() {
  $schema['makemeeting_answers'] = array(
    'description' => 'The choices by the visitors.',
    'fields' => array(
      'answer_id' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'field_name' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'description' => 'The name of the field this data is attached to',
      ),
      'entity_type' => array(
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The entity type this data is attached to',
      ),
      'deleted' => array(
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'A boolean indicating whether this data item has been deleted'
      ),
      'entity_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The entity id this data is attached to',
      ),
      'language' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The language for this data item.',
      ),
      'delta' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The sequence number for this data item, used for multi-value fields',
      ),
      'value' => array(
        'type' => 'text',
        'serialize' => TRUE,
      ),
      'uid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 255,
      ),
      'mail' => array(
        'type' => 'varchar',
        'length' => 255,
      ),
      'notification' => array(
        'type' => 'int',
        'not null' => TRUE,
        'length' => 1,
        'default' => 0,
      ),
    ),
    'primary key' => array('answer_id'),
  );
  return $schema;
}

/**
 * Add yesnomaybe column to field tables
 */
function makemeeting_update_7201() {
  $results = db_select('field_config', 'fc')
             ->fields('fc')
             ->condition('type', 'makemeeting')
             ->execute()
             ->fetchAll();
  foreach ($results as $field) {
    foreach (array('data', 'revision') as $type) {
      $table = "field_{$type}_{$field->field_name}";
      $column = "{$field->field_name}_yesnomaybe";
      // Data and revision tables need to be fixed
      if (!db_field_exists($table, $column)) {
        db_add_field($table, $column, array(
          'type' => 'int',
          'length' => 1,
          'not null' => FALSE,
          'default' => 0,
        ));
      }
    }
  }
}

/**
 * Returns all fields created on the system of the type defined in makemeeting.
 */
function makemeeting_get_makemeeting_fields() {
  $types = array_keys(makemeeting_field_info());
  $fields = [];
  foreach (field_info_fields() as $field) {
    if (in_array($field['type'], $types)) {
      $fields[] = $field;
    }
  }
  return $fields;
}

/**
 * Add timezone column to fields.
 */
function makemeeting_update_7202() {
  $fields = makemeeting_get_makemeeting_fields();

  $schema = makemeeting_field_schema(NULL);
  $spec = $schema['columns']['timezone'];

  foreach ($fields as $field) {
    $table_prefixes = [
      _field_sql_storage_tablename($field),
      _field_sql_storage_revision_tablename($field)
    ];
    foreach ($table_prefixes as $table) {
      $field_name = $field['field_name'];
      $column = $field_name . '_timezone';
      db_add_field($table, $column, $spec);
    }
  }
  return t('Timezone column added to makemeeting forms.');
}

/**
 * Migrate answers to date keys so that we can handle timezones
 */
function makemeeting_update_7203(&$sandbox){
  if (!isset($sandbox['progress'])) {
    $sandbox['progress'] = 0;
    $sandbox['current_aid'] = 0;
    $sandbox['max'] = db_query('SELECT COUNT(DISTINCT answer_id) FROM {makemeeting_answers}')->fetchField();
  }

  $answers = db_select('makemeeting_answers', 'a')
    ->fields('a', ['answer_id', 'value'])
    ->condition('answer_id', $sandbox['current_aid'], '>')
    ->range(0, 10)
    ->orderBy('answer_id', 'ASC')
    ->execute()
    ->fetchAllKeyed();

  foreach ($answers as $answer_id => $data) {
    $values = unserialize($data);
    $new_value = array();
    foreach ($values as $key => $value) {
      list($timestamp, $rest) = explode(':', $key, 2);
      $date = DateTime::createFromFormat('U', $timestamp);
      $new_value[$date->format('d-m-Y') . ':' . $rest] = $value;
    }
    db_update('makemeeting_answers')
      ->fields(['value' => serialize($new_value)])
      ->condition('answer_id', $answer_id)
      ->execute();

    $sandbox['progress']++;
    $sandbox['current_aid'] = $answer_id;
  }

  $sandbox['#finished'] = empty($sandbox['max']) ? 1 : ($sandbox['progress'] / $sandbox['max']);

  // To display a message to the user when the update is completed, return it.
  // If you do not want to display a completion message, simply return nothing.
  return t('Migrate answers to date format.');
}

/**
 * Migrate questions to date keys so that we can handle timezones
 */
function makemeeting_update_7204(&$sandbox){
  $fields = makemeeting_get_makemeeting_fields();

  foreach ($fields as $field) {
    $table_prefixes = [
      _field_sql_storage_tablename($field),
      _field_sql_storage_revision_tablename($field)
    ];
    foreach ($table_prefixes as $table) {
      $items = db_select($table, 'f')
        ->fields('f')
        ->execute();

      foreach ($items as $item) {
        $field_prefix = $field['field_name'] . '_';

        // convert value to date key
        $choices = unserialize($item->{$field_prefix . 'choices'});
        $new_choices = array();
        foreach ($choices as $key => $choice) {
          $date = DateTime::createFromFormat('U', $key);
          $new_choices[$date->format('d-m-Y')] = $choice;
        }

        // initialize timezone given author
        $tz = drupal_get_user_timezone();
        $entity = reset(entity_load($item->entity_type, [$item->entity_id]));
        if (variable_get('configurable_timezones', 1)) {
          if (property_exists($entity, 'uid') && $entity->uid) {
            $account = user_load($entity->uid);
            $tz = $account->timezone;
          }
          elseif (property_exists($entity, 'uid')) {
            $account = user_load($entity->uid);
            $tz = $account->timezone;
          }
        }
        db_update($table)
          ->fields([
            $field_prefix . 'choices'  => serialize($new_choices),
            $field_prefix . 'timezone' => $tz,
          ])
          ->condition('entity_type', $item->entity_type)
          ->condition('entity_id', $item->entity_id)
          ->condition('revision_id', $item->revision_id)
          ->condition('language', $item->language)
          ->condition('delta', $item->delta)
          ->execute();
      }
    }
  }
  return t('Timezone column added to makemeeting forms.');
}
