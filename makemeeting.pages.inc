<?php


/**
 * Menu callback: delete an answer
 *
 * @param $form
 * @param $form_state
 * @param $answer
 * @param bool $redirect
 * @return array
 */
function makemeeting_delete_answer($form, &$form_state, $answer, $redirect = TRUE) {
  $form['#answer'] = $answer;
  return confirm_form(
    $form,
    t('Are you sure you want to delete the answer?'),
    ''
  );
}

/**
 * Form callback: confirm answer deletion
 */
function makemeeting_delete_answer_submit($form, &$form_state) {
  $answer = $form['#answer'];
  db_delete('makemeeting_answers')
    ->condition('answer_id', $answer->answer_id)
    ->execute();

  _makemeeting_clear_related_entity_cache($answer->entity_type, $answer->entity_id);
}

/**
 * Menu callback: edit an answer
 *
 * @param object $answer
 *  The loaded answer
 * @param string $type
 *  Ajax or nojs
 * @return array|void
 *  The entire form or just the row depending on ajax
 */
function makemeeting_edit_answer($answer, $type = 'ajax') {
  $entities = entity_load($answer->entity_type, [$answer->entity_id]);
  if (empty($entities)) {
    return drupal_not_found();
  }
  $entity = $entities[$answer->entity_id];
  $field = $entity->{$answer->field_name}[$answer->language][$answer->delta];
  $form = drupal_get_form('makemeeting_answers_form_' . $answer->entity_id, $field, (array) $answer, $answer);
  if ($type == 'ajax') {
    $form['#theme'] = 'makemeeting_answer_row';
    // Replace 'ajax' word in form action
    $form['#action'] = str_replace('/ajax/', '/nojs/', $form['#action']);
    $colspan = 1 + count(element_children($form['answers']));
    $output = '<tr class="editing"><td colspan="' . $colspan . '">' . drupal_render($form) . '</td></tr>';
    $commands = [];
    // See ajax_example_advanced.inc for more details on the available commands
    // and how to use them.
    $commands[] = ajax_command_replace('#answer-' . $answer->answer_id, $output);
    $page = ['#type' => 'ajax', '#commands' => $commands];
    return ajax_deliver($page);
  }
  return $form;
}
